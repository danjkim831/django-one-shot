from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.


# views for ALL the todo-lists:
def todo_list_list(request):
    all_lists = TodoList.objects.all()
    context = {
        "todo_list_list": all_lists,
    }
    return render(request, "todos/list.html", context)


# views for the details of a single particular to-do list:
def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/detail.html", context)


# view for CREATING a new TODO LIST:
def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    elif request.method == "GET":
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


# view to EDIT/UPDATE a specific todo list:
def todo_list_update(request, id):
    post = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save()
            return redirect("todo_list_detail", id=id)
    elif request.method == "GET":
        form = TodoListForm(instance=post)

    context = {
        # "form": post,
        "todo_form": form,
    }
    return render(request, "todos/edit.html", context)


# view to DELETE a specific todo list:
def todo_list_delete(request, id):
    form = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


# view to CREATE a new TODO ITEM:
def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    elif request.method == "GET":
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/items_create.html", context)


# view to EDIT/UPDATE an existing TODO ITEM:
def todo_item_update(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)

        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=post.list.id)
    elif request.method == "GET":
        form = TodoItemForm(instance=post)

    context = {
        "form": form,
    }
    return render(request, "todos/items_edit.html", context)
