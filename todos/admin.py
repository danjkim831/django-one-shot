from django.contrib import admin
from todos.models import TodoList, TodoItem


# Register your models here.


# Registering TodoList model into admin:
@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
    ]


# Registering the TodoItem model to admin:
@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = [
        "task",
        "due_date",
        "is_completed",
    ]
